# IrotokPy (Irotok in Python)

### OAuth Flow
1. **POST oauth/request_token**
  - `(Callback, ConsumerKey, ConsumerSecret) -> (RequestToken, RequestTokenSecret)`
2. **GET oauth/authenticate** or **GET oauth/authorize**
  - `RequestToken -> Unit`
  - Side effect: Open the Twitter webpage for approval.
  - authenticate: No re-approval for *Use Sign in with Twitter*
  - authorize: Re-approval always (Desktop applications must use this method.)
3. Approve in the webpage
  - `Unit -> Either<OAuthVerifier, PIN>`
4. **POST oauth/access_token**
  - `(Either<OAuthVerifier, PIN>, ConsumerKey, ConsumerSecret, RequestToken, RequestTokenSecret) -> (AccessToken, AccessTokenSecret, UserId, Username)`