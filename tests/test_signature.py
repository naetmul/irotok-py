from irotokpy.common import HttpMethod
from irotokpy.signature import create_signature


# Use the example in https://developer.twitter.com/en/docs/basics/authentication/guides/creating-a-signature
def test_create_signature():
    http_method = HttpMethod.POST
    base_url = "https://api.twitter.com/1.1/statuses/update.json"

    params = {
        "status": "Hello Ladies + Gentlemen, a signed OAuth request!",
        "include_entities": True,
        "oauth_consumer_key": "xvz1evFS4wEEPTGEFPHBog",
        "oauth_nonce": "kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg",
        "oauth_signature_method": "HMAC-SHA1",
        "oauth_timestamp": 1318622958,
        "oauth_token": "370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb",
        "oauth_version": "1.0"
    }

    consumer_secret = "kAcSOqF21Fu85e7zjz7ZN2U4ZRhfV3WpwPAoE3Z7kBw"
    token_secret = "LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE"

    expected = "hCtSmYh+iHYCEqBWrE7C7hYmtUk="

    assert create_signature(http_method, base_url, params, consumer_secret, token_secret) == expected
