import base64
import random
import re
import time
from enum import Enum, auto
from typing import Any, Dict, Optional

from irotokpy.common import HttpMethod, percent_encode
from irotokpy.signature import create_signature


class AccessLevel(Enum):
    READ = auto()
    WRITE = auto()


# Authorizing a request
# "Authorization" item in HTTP header
def get_authorization_header(http_method: HttpMethod, base_url: str, params: Dict[str, Any],
                             consumer_key: str, consumer_secret: str,
                             token: Optional[str] = None, token_secret: Optional[str] = None) -> str:
    nonce = _generate_nonce()
    signature_method = "HMAC-SHA1"
    timestamp = int(time.time())
    version = "1.0"

    new_params = {
        "oauth_consumer_key": consumer_key,
        "oauth_nonce": nonce,
        "oauth_signature_method": signature_method,
        "oauth_timestamp": timestamp,
        "oauth_version": version
    }
    if token is not None:
        new_params["oauth_token"] = token

    signature = create_signature(http_method, base_url, {**params, **new_params}, consumer_secret, token_secret)
    new_params["oauth_signature"] = signature

    return _build_header_string(new_params)


# base64 encoding 32 bytes of random data, and stripping out all non-word characters
def _generate_nonce() -> str:
    # 32 bytes of random data
    random_integers = random.choices(range(256), k=32)
    random_bytes = bytes(random_integers)

    # base64 encoding
    encoded_bytes = base64.b64encode(random_bytes)
    encoded_string = encoded_bytes.decode()

    # stripping out all non-word characters
    stripped = re.sub("[^A-Za-z0-9]", "", encoded_string)

    return stripped


def _build_header_string(param_dict: Dict[str, Any]) -> str:
    # Dict to List
    param_list = list(map(lambda pair: (pair[0], str(pair[1])), param_dict.items()))
    param_list.sort(key=lambda pair: pair[0])

    # Append the string “OAuth ” (including the space at the end) to DST.
    dst = "OAuth "
    # For each key/value pair of the 7 parameters listed above:
    for index, (key, value) in enumerate(param_list):
        # Percent encode the key and append it to DST.
        encoded_key = percent_encode(key)
        dst = dst + encoded_key
        # Append the equals character ‘=’ to DST.
        dst = dst + "="
        # Append a double quote ‘”’ to DST.
        dst = dst + '"'
        # Percent encode the value and append it to DST.
        encoded_value = percent_encode(value)
        dst = dst + encoded_value
        # Append a double quote ‘”’ to DST.
        dst = dst + '"'
        # If there are key/value pairs remaining, append a comma ‘,’ and a space ‘ ‘ to DST.
        if index < len(param_list) - 1:
            dst = dst + ", "

    return dst
