from typing import Optional

import requests

from irotokpy.authorization import get_authorization_header
from irotokpy.common import HttpMethod, query_string_to_single_value_dict
from irotokpy.twitter_response import TwitterResponse, TwitterError


# POST oauth/access_token
def access_token(oauth_verifier: str,
                 consumer_key: str, consumer_secret: str,
                 request_token: str, request_token_secret: str) -> Optional[TwitterResponse]:
    http_method = HttpMethod.POST
    base_url = "https://api.twitter.com/oauth/access_token"

    # Set parameters.
    params = {
        "oauth_verifier": oauth_verifier
    }

    # Set headers.
    authorization_header = get_authorization_header(http_method, base_url, params, consumer_key, consumer_secret,
                                                    request_token, request_token_secret)
    headers = {
        "Authorization": authorization_header
    }

    try:
        response = requests.post(base_url, headers=headers, data=params)
    except Exception as ex:
        print(type(ex))
        print(ex)
        return None

    status_code = response.status_code
    text = response.text

    print("Response code:", status_code)

    # Success
    # oauth_token=1234-AAAAXXXXXXXXZZZZZZZZ999999Aaaaaa00000000&oauth_token_secret=aaaaAAAAZZZZZZZZZZxxxxxx999999200000987799WWW&user_id=1234&screen_name=Thunder
    #
    # Failure
    # Reverse auth credentials are invalid
    # or
    # 이 기능은 일시적으로 사용할 수 없습니다.
    print("Response text:", text)

    if status_code == 200:
        # Success
        # response type: query string
        response_dict = query_string_to_single_value_dict(text)
        return TwitterResponse(status_code, response_dict, [])

    else:
        # Failure
        # response type: text
        error = TwitterError(None, response.text)
        return TwitterResponse(status_code, None, [error])


'''
# Example

import irotokpy.PrivateConstants as Constants

print(access_token("ZZZZZZZZZZZZZZZZzzzzzzzzzzzAA000", Constants.CONSUMER_KEY, Constants.CONSUMER_SECRET,
                   "aaaAaaAAAAAAhlGwAAAAAAAaaAa", "AAAaaaa0000dAaAAAoAAadA7MeSqFAaa"))
'''
