from dataclasses import dataclass
from typing import Optional, Tuple

from irotokpy.api.authenticate import authenticate
from irotokpy.api.authorize import authorize
from irotokpy.api.request_token import request_token
from irotokpy.authorization import AccessLevel


class OAuthCallbackType:
    pass


@dataclass
class OAuthCallbackEmpty(OAuthCallbackType):
    pass


@dataclass
class OAuthCallbackUrl(OAuthCallbackType):
    url: str


@dataclass
class OAuthCallbackPin(OAuthCallbackType):
    pass


# shortcut method (not a part of Twitter API)
def auth(oauth_callback: OAuthCallbackType, re_approve: bool, consumer_key: str, consumer_secret: str,
         x_auth_access_type: Optional[AccessLevel],
         force_login: Optional[bool], prefilled_username: Optional[str]) -> Tuple[str, str]:
    if isinstance(oauth_callback, OAuthCallbackEmpty):
        callback = ""
    elif isinstance(oauth_callback, OAuthCallbackUrl):
        callback = oauth_callback.url
    elif isinstance(oauth_callback, OAuthCallbackPin):
        callback = "oob"  # out-of-band pin mode
    else:
        raise NotImplementedError

    token_req = request_token(callback, x_auth_access_type, consumer_key, consumer_secret)
    token = token_req.value["oauth_token"]
    token_secret = token_req.value["oauth_token_secret"]

    if re_approve:
        authorize(token, force_login, prefilled_username)
    else:
        authenticate(token, force_login, prefilled_username)

    return token, token_secret


'''
# Example
import irotokpy.PrivateConstants as Constants

print(auth(OAuthCallbackUrl(Constants.CALLBACK_URL), False, Constants.CONSUMER_KEY, Constants.CONSUMER_SECRET, None, False, None))
'''
