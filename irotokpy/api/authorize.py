import urllib.parse as urlparse
import webbrowser
from typing import Optional

from irotokpy.common import HttpMethod


# Launches the browser
# For Desktop applications (Re-approve for granted user)
def authorize(request_token: str, force_login: Optional[bool], prefilled_username: Optional[str]) -> None:
    http_method = HttpMethod.GET
    base_url = "https://api.twitter.com/oauth/authorize"

    # Set parameters.
    params = {
        "oauth_token": request_token
    }
    if force_login is True:
        params["force_login"] = "true"
    if prefilled_username is not None:
        params["screen_name"] = prefilled_username

    webbrowser.open(base_url + "?" + urlparse.urlencode(params))


'''
# Example

import irotokpy.PrivateConstants as Constants
from irotokpy.api.request_token import request_token
from irotokpy.authorization import AccessLevel

token_req = request_token("oob", AccessLevel.WRITE, Constants.CONSUMER_KEY, Constants.CONSUMER_SECRET)
token = token_req.value["oauth_token"]
token_secret = token_req.value["oauth_token_secret"]
authorize(token, True, "TestName")
'''
