from typing import Optional

import requests

from irotokpy.authorization import get_authorization_header
from irotokpy.common import HttpMethod, json_to_dict, json_to_list
from irotokpy.twitter_response import TwitterResponse, TwitterError


# GET statuses/home_timeline
def home_timeline(consumer_key: str, consumer_secret: str, token: str, token_secret: str,
                  count: Optional[int] = None,  # <= 200
                  since_id: Optional[str] = None, max_id: Optional[str] = None,
                  trim_user: Optional[bool] = None, exclude_replies: Optional[bool] = None,
                  include_entities: Optional[bool] = None):
    http_method = HttpMethod.GET
    base_url = "https://api.twitter.com/1.1/statuses/home_timeline.json"

    # Set parameters.
    params = {}
    if count is not None:
        params["count"] = count
    if since_id is not None:
        params["since_id"] = since_id
    if max_id is not None:
        params["max_id"] = max_id
    if trim_user is True:
        params["trim_user"] = "true"
    if exclude_replies is True:
        params["exclude_replies"] = "true"
    if include_entities is False:
        params["include_entities"] = "false"

    # Set headers.
    authorization_header = get_authorization_header(http_method, base_url, params, consumer_key, consumer_secret,
                                                    token, token_secret)
    headers = {
        "Authorization": authorization_header
    }

    try:
        response = requests.get(base_url, headers=headers, data=params)
    except Exception as ex:
        print(type(ex))
        print(ex)
        return None

    status_code = response.status_code
    text = response.text

    print("Response code:", status_code)

    # JSON
    print("Response text:", text)

    if status_code == 200:
        # Success
        # response type: JSON
        response_list = json_to_list(text)
        return TwitterResponse(status_code, response_list, [])

    else:
        # Failure
        # response type: JSON
        error_dict = json_to_dict(text)

        errors = list(map(lambda error: TwitterError(error["code"], error["message"]), error_dict["errors"]))
        return TwitterResponse(status_code, None, errors)


'''
# Example

import irotokpy.PrivateConstants as Constants

print(home_timeline(Constants.CONSUMER_KEY, Constants.CONSUMER_SECRET, Constants.ACCESS_TOKEN, Constants.ACCESS_TOKEN_SECRET))
'''
