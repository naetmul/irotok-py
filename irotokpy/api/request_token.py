from typing import Optional

import requests

from irotokpy.authorization import get_authorization_header, AccessLevel
from irotokpy.common import HttpMethod
from irotokpy.common import ResponseType, query_string_to_single_value_dict, json_to_dict, xml_to_dict
from irotokpy.twitter_response import TwitterResponse, TwitterError


# POST oauth/request_token
def request_token(oauth_callback: str, x_auth_access_type: Optional[AccessLevel],
                  consumer_key: str, consumer_secret: str) -> Optional[TwitterResponse]:
    http_method = HttpMethod.POST
    base_url = "https://api.twitter.com/oauth/request_token"

    # Set parameters.
    params = {
        "oauth_callback": oauth_callback
    }
    if x_auth_access_type is AccessLevel.READ:
        params["x_auth_access_type"] = "read"
    elif x_auth_access_type is AccessLevel.WRITE:
        params["x_auth_access_type"] = "write"

    # Set headers.
    authorization_header = get_authorization_header(http_method, base_url, params, consumer_key, consumer_secret)
    headers = {
        "Authorization": authorization_header
    }

    try:
        response = requests.post(base_url, headers=headers, data=params)
    except Exception as ex:
        print(type(ex))
        print(ex)
        return None

    status_code = response.status_code
    text = response.text

    print("Response code:", status_code)

    # Success
    # oauth_token=aaaAaaAAAAAAhlGwAAAAAAAaaAa&oauth_token_secret=AAAaaaa0000dAaAAAoAAadA7MeSqFAaa&oauth_callback_confirmed=true
    #
    # Failure
    # {"errors":[{"code":32,"message":"Could not authenticate you."}]}
    # or
    # <?xml version="1.0" encoding="UTF-8"?><errors><error code="415">Callback URL not approved for this client application. Approved callback URLs can be adjusted in your application settings</error></errors>
    print("Response text:", text)

    if status_code == 200:
        # Success
        # response type: query string
        response_dict = query_string_to_single_value_dict(text)
        return TwitterResponse(status_code, response_dict, [])

    else:
        # Failure
        # response type: JSON or XML
        text = response.text
        response_type = ResponseType.infer(text)
        if response_type is ResponseType.JSON:
            error_dict = json_to_dict(text)
        elif response_type is ResponseType.XML:
            error_dict = xml_to_dict(text)
        else:
            raise NotImplementedError

        errors = list(map(lambda error: TwitterError(error["code"], error["message"]), error_dict["errors"]))
        return TwitterResponse(status_code, None, errors)


'''
# Example

import irotokpy.PrivateConstants as Constants

result = request_token(Constants.CALLBACK_URL, AccessLevel.WRITE, Constants.CONSUMER_KEY, Constants.CONSUMER_SECRET)
print(result)
'''
