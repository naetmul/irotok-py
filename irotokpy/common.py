import json
import re
from enum import Enum, auto
from typing import Any, Dict, List, Tuple, Union
from urllib.parse import parse_qs, parse_qsl
from xml.etree import ElementTree

import requests


class HttpMethod(Enum):
    GET = auto()
    POST = auto()


class ResponseType(Enum):
    QUERY_STRING = auto()
    JSON = auto()
    XML = auto()

    @staticmethod
    def infer(string: str) -> 'ResponseType':
        if ResponseType.is_json(string):
            return ResponseType.JSON
        elif ResponseType.is_xml(string):
            return ResponseType.XML
        else:
            return ResponseType.QUERY_STRING

    @staticmethod
    def is_json(string: str) -> bool:
        # true iff `string` starts with any number of whitespaces and then { or [
        return re.match("\s*[{[]", string) is not None

    @staticmethod
    def is_xml(string: str) -> bool:
        # true iff `string` starts with any number of whitespaces and then <
        return re.match("\s*<", string) is not None


def percent_encode(s: str) -> str:
    # Percent encode '/' also.
    return requests.utils.quote(s, safe="")


def query_string_to_single_value_dict(query_string: str) -> Dict[str, str]:
    pair_list: List[Tuple[str, str]] = parse_qsl(query_string)
    return dict(pair_list)


def query_string_to_multi_value_dict(query_string: str) -> Dict[str, List[str]]:
    return parse_qs(query_string)


def json_to_dict(json_string: str) -> Dict[str, Any]:
    dict_or_list = json.loads(json_string)
    if not isinstance(dict_or_list, dict):
        raise TypeError("Type of the json is " + str(type(dict_or_list)))
    return dict_or_list


def json_to_list(json_string: str) -> Union[List[List], List[Dict[str, Any]]]:
    dict_or_list = json.loads(json_string)
    if not isinstance(dict_or_list, list):
        raise TypeError("Type of the json is " + str(type(dict_or_list)))
    return dict_or_list


def xml_to_dict(xml_string: str) -> Dict[str, Any]:
    root = ElementTree.fromstring(xml_string)

    if root.tag == "errors":
        errors = []
        for error_tag in root.findall("./error"):
            code = int(error_tag.get("code"))
            msg = "".join(error_tag.itertext())

            errors.append({"code": code, "message": msg})

        return {
            "errors": errors
        }
    else:
        raise NotImplementedError("XML with <" + root.tag + "> TAG")
