from typing import Any, List, Optional
from dataclasses import dataclass


@dataclass
class TwitterError:
    code: Optional[int]
    message: str


@dataclass
class TwitterResponse:
    status_code: int
    value: Any
    errors: List[TwitterError]