from typing import Optional, Dict, Any

import base64
import hmac
from irotokpy.common import HttpMethod, percent_encode


# https://developer.twitter.com/en/docs/basics/authentication/guides/creating-a-signature.html
def create_signature(http_method: HttpMethod, base_url: str, params: Dict[str, Any],
                     consumer_secret: str, token_secret: Optional[str] = None) -> str:
    str_params = _params_to_str_dict(params)

    parameter_string = _get_parameter_string(str_params)
    signature_base_string = _get_signature_base_string(http_method, base_url, parameter_string)
    signing_key = _get_signing_key(consumer_secret, token_secret)
    signature = _calc_signature(signature_base_string, signing_key)

    return signature


def _get_parameter_string(params: Dict[str, str]) -> str:
    output_str = ""

    # Percent encode every key and value that will be signed.
    param_list = []
    for key, value in params.items():
        percent_encoded_key = percent_encode(key)
        percent_encoded_value = percent_encode(value)
        param_list.append((percent_encoded_key, percent_encoded_value))

    # Sort the list of parameters alphabetically by encoded key.
    param_list.sort(key=lambda pair: pair[0])

    # For each key/value pair
    for index, pair in enumerate(param_list):
        # Append the encoded key to the output string.
        output_str = output_str + pair[0]
        # Append the ‘=’ character to the output string.
        output_str = output_str + "="
        # Append the encoded value to the output string.
        output_str = output_str + pair[1]
        # If there are more key/value pairs remaining, append a ‘&’ character to the output string.
        if index < len(param_list) - 1:
            output_str = output_str + "&"

    return output_str


def _get_signature_base_string(http_method: HttpMethod, base_url: str, parameter_string: str) -> str:
    # Convert the HTTP Method to uppercase and set the output string equal to this value.
    if http_method is HttpMethod.GET:
        http_method_str = "GET"
    elif http_method is HttpMethod.POST:
        http_method_str = "POST"
    else:
        raise NotImplementedError
    output_str = http_method_str

    # Append the ‘&’ character to the output string.
    output_str = output_str + "&"

    # Percent encode the URL and append it to the output string.
    percent_encoded_url = percent_encode(base_url)
    output_str = output_str + percent_encoded_url

    # Append the ‘&’ character to the output string.
    output_str = output_str + "&"

    # Percent encode the parameter string and append it to the output string.
    percent_encoded_parameter_string = percent_encode(parameter_string)
    output_str = output_str + percent_encoded_parameter_string

    return output_str


def _get_signing_key(consumer_secret: str, token_secret: Optional[str] = None) -> str:
    percent_encoded_consumer_secret = percent_encode(consumer_secret)
    if token_secret is not None:
        percent_encoded_token_secret = percent_encode(token_secret)
        signing_key = percent_encoded_consumer_secret + "&" + percent_encoded_token_secret
    else:
        signing_key = percent_encoded_consumer_secret + "&"
    return signing_key


def _calc_signature(signature_base_string: str, signing_key: str) -> str:
    # Passing the signature base string and signing key to the HMAC-SHA1 hashing algorithm.
    key_bytes = signing_key.encode()
    msg_bytes = signature_base_string.encode()
    hmac_obj = hmac.new(key_bytes, msg_bytes, "sha1")
    signature_bytes = hmac_obj.digest()

    # This needs to be base64 encoded to produce the signature string.
    encoded_bytes = base64.b64encode(signature_bytes)
    signature = encoded_bytes.decode()

    return signature


def _params_to_str_dict(params: Dict[str, Any]) -> Dict[str, str]:
    converted = {}
    for key, value in params.items():
        if value is None:
            pass
        elif isinstance(value, bool):
            if value:
                converted[key] = "true"
            else:
                converted[key] = "false"
        else:
            converted[key] = str(value)

    return converted
